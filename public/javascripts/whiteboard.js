class ToolBase{
    constructor(model, whiteboardId){
        this.model = model;
        this.whiteboardId = whiteboardId;
    }
   
    handleOnClick(params){
        //virtual return true if continue using the tool
    }
    handleOnMouseDown(params){
        //virtual return true if continue using the tool
    }
    handleOnMouseUp(params){
        //virtual return true if continue using the tool
    }

    occupiesLocation(x,y){
        return false;
    }
}

class PointerTool extends ToolBase{
    constructor(model, whiteboardId){
        super(model, whiteboardId);
        this.toolName = 'Pointer';
    }

    handleOnMouseUp(params){
        return true;
    }
}

class DrawStickyTool extends ToolBase{
    constructor(model, whiteboardId){
        super(model, whiteboardId);
        this.toolName = 'Sticky';
    }

    handleOnMouseUp(params){
        let sticky = document.createElement('div');
        document.getElementById(this.whiteboardId).appendChild(sticky);
        let left = params.offsetX - 20;
        let top = params.offsetY - 20;
        sticky.className = 'sticky';
        sticky.style.left = left+ 'px';
        sticky.style.top = top + 'px';
        sticky.style.backgroundColor = sticky.color;
        sticky.textContent = '';
        sticky.contentEditable=true;
        sticky.onmouseup = this.onStickyMouseUp;
        sticky.onblur = this.onStickyBlur;
        sticky.ondblclick = this.onStickyDblClick;
        sticky.id = 'whiteboardsticky' + this.model.getNextId();
        this.preventNextDrag = false;
        this.dragElement(sticky);
        
        this.model.stickies.push(
            new Sticky({
                x: left,
                y: top,
                color: sticky.color,
                id: sticky.id
            })
        );

        let jsticky = $(sticky);
        jsticky.trigger('focus');
        return false;    
    }

    onStickyDblClick(e){
        console.log('dblclick');
        e.target.contentEditable = true;
        this.preventNextDrag = true;
        e.target.focus();
    }
    onStickyBlur(e){
        e.target.contentEditable = false;
        this.preventNextDrag = false;
        console.log('blur');
    }

    dragElement(elmnt) {
        var pos1 = 0, pos2 = 0, pos3 = 0, pos4 = 0;
        elmnt.onmousedown = dragMouseDown;
    
      
        function dragMouseDown(e) {
            console.log('dragMouseDown');
            if(this.preventNextDrag){
                console.log('preventDrag');
                this.preventNextDrag = false;
                return;
            }
            if(document.activeElement){
                document.activeElement.blur();
            }
            console.log('drag');
            e = e || window.event;
         
            e.preventDefault();
            
            // get the mouse cursor position at startup:
            pos3 = e.clientX;
            pos4 = e.clientY;
            document.onmouseup = closeDragElement;
            // call a function whenever the cursor moves:
            document.onmousemove = elementDrag;
    
        }
      
        function elementDrag(e) {
            e = e || window.event;
            e.preventDefault();
            // calculate the new cursor position:
            pos1 = pos3 - e.clientX;
            pos2 = pos4 - e.clientY;
            pos3 = e.clientX;
            pos4 = e.clientY;
            // set the element's new position:
            elmnt.style.top = (elmnt.offsetTop - pos2) + "px";
            elmnt.style.left = (elmnt.offsetLeft - pos1) + "px";
        }
      
        function closeDragElement(e) {
            // stop moving when mouse button is released:
            document.onmouseup = null;
            document.onmousemove = null;
            
        }
      }
}

class WhiteboardObject{
    constructor(params){

    }

    occupiesLocation(x,y){
        return false;
    }

    handleOnMouseDown(params){

    }
    handleOnMouseUp(params){

    }
    handleOnClick(params){

    }
}

class Sticky extends WhiteboardObject{
    constructor(params){
        super(params);
        this.x = params.x;
        this.y = params.y;
        this.width = 150;
        this.height = 150;
        this.z = 0;
        this.color= params.color;
        this.border = '#333333';    
        this.id = params.id;
    }

    occupiesLocation(x,y){
        return x >= this.x && x <= this.x + this.width && y>=this.y && y<=this.y + this.height;
    }

    handleOnMouseDown(params){
        this.color = '#aaaacc';
    }
}

class Tools {

    constructor(model, whiteboardId){
        this.tools = new Array();
        
        this.drawStickyTool = new DrawStickyTool(model, whiteboardId);
        this.pointerTool = new PointerTool(model, whiteboardId);
        this.tools.push(this.drawStickyTool);
        this.tools.push(this.pointerTool);
    }

    static get tools(){
        return tools;
    }
}

class WhiteboardDataModel{
    constructor(){
        this.stickies = Array();
        this.nextId = 1;
    }

    getNextId(){
        return this.nextId++;
    }

    reset(){
        this.stickies = Array();
    }
}

class Whiteboard { 

    constructor(canvasId, overlayId, onActiveToolChanged){
        this.modelRefreshPeriod=1000;
        this.htmlCanvas = document.getElementById('whiteboard');
        this.overlay = document.getElementById('overlay');
        this.context = this.htmlCanvas.getContext('2d');
        this.container = this.htmlCanvas.parentElement;
        this.model = new WhiteboardDataModel();
        this.resetModel();
        this.activeTool = null;
        this.onActiveToolChanged = onActiveToolChanged;

    };

    initialize() {
        window.addEventListener('resize', this.resizeCanvas.bind(this), false);
        this.htmlCanvas.addEventListener('click', this.onClick.bind(this), true);
        this.htmlCanvas.addEventListener('mousedown', this.onMouseDown.bind(this), true);
        this.htmlCanvas.addEventListener('mouseup', this.onMouseUp.bind(this), true);
        
        this.resizeCanvas();
        setTimeout(this.updateModelFromOverlay, this.modelRefreshPeriod, this.updateModelFromOverlay, this.modelRefreshPeriod);        
    }

    updateModelFromOverlay(thisMethod, refreshPeriod){
        whiteboard.model.stickies.forEach((sticky)=>{
            var element = $('#'+sticky.id)[0];
            if(element ){
                sticky.x = element.style.left;
                sticky.y = element.style.top;
                sticky.color = element.style.backgroundColor;
            }
            else{
                console.warn('sticky ' + sticky.id + ' got lost!');
            }
        });
        setTimeout(function(){thisMethod(thisMethod, refreshPeriod)},refreshPeriod);
    }
    
    onMouseDown(params){
        console.log('md');
        var clickedObject = this.getTopObjectOnLocation(params.offsetX, params.offsetY);
        if(clickedObject){
            if(!clickedObject.handleOnMouseDown(params)){
                this.setActiveTool(tools.pointerTool);
            }
        }
        else{
            this.activeTool.handleOnMouseDown(params);
        }
        this.redraw();
    }

    onMouseUp(params){
        console.log('mu');
        var clickedObject = this.getTopObjectOnLocation(params.offsetX, params.offsetY);
        if(clickedObject){
            if(!clickedObject.handleOnMouseUp(params)){
                this.setActiveTool(tools.pointerTool);
            }
        }
        else{
            this.activeTool.handleOnMouseUp(params);
        }
        this.redraw();
    }

    onClick(params){
        console.log('cl');
        var clickedObject = this.getTopObjectOnLocation(params.offsetX, params.offsetY);
        if(clickedObject){
            if(!clickedObject.handleOnClick(params)){
                this.setActiveTool(tools.pointerTool);
            }
        }
        else{
            this.activeTool.handleOnClick(params);
        }
        this.redraw();
    }

    getTopObjectOnLocation(x,y){
        let selected = null;
        this.model.stickies.forEach((sticky)=>{
            if(sticky.occupiesLocation(x,y) && (selected == null || sticky.z > selected.z)){
                selected = sticky; 
            }
        });
        return selected;
    }

    redraw() {
        if(!this.model)
            return;
    }

    resizeCanvas() {
        this.htmlCanvas.width = this.container.clientWidth;
        this.htmlCanvas.height = this.container.clientHeight;
        this.redraw();
    }

    setActiveTool(tool){
        this.activeTool = tool;
        this.activeTool.model = this.model;
        if(this.onActiveToolChanged){
            this.onActiveToolChanged();
        }
    }

    resetModel(){
       this.model.reset();
    }

    clear(){
        this.context.clearRect(0, 0, this.htmlCanvas.width, this.htmlCanvas.height);
        this.resetModel();
        while(this.overlay.firstChild){
            this.overlay.removeChild(this.overlay.firstChild);
        }
        this.redraw();
        this.setActiveTool(this.activeTool);
    }
    
};


let whiteboard = new Whiteboard('whiteboard', 'overlay');
whiteboard.initialize();
let tools = new Tools(whiteboard.model, 'overlay');