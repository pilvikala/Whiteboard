var whiteboardButtons = {
    erase : function(){
        whiteboard.clear();
    },

    enableStickyNoteTool : function(){
        whiteboard.setActiveTool(tools.drawStickyTool);
    },

    enablePointerTool:function(){
        whiteboard.setActiveTool(tools.pointerTool);
    }
};

document.onkeyup = function(e){
    if(document.activeElement.tagName != 'BODY'){
        return;
    }

    if(e.key == 'n'){
        $('#StickyButton').trigger('click');
    }
    if(e.key == 'c'){
        $('#ClearButton').trigger('click');
    }
    if(e.key == 'a'){
        $('#PointerButton').trigger('click');
    }
}

whiteboard.onActiveToolChanged = function(){
    var btnId = whiteboard.activeTool.toolName + 'Button';
    $('.toolButton').removeClass('btn-light');
    $('.toolButton').addClass('btn-outline-light');
    
    $('#' + btnId).addClass('btn-light');
    $('#'+btnId).removeClass('btn-outline-light');
}

whiteboard.setActiveTool(tools.pointerTool);